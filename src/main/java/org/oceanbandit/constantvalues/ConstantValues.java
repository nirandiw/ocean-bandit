package org.oceanbandit.constantvalues;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by workshop on 27/11/2014.
 */
public class ConstantValues {
    public static final double ALPHA_T = 0.4;
    public static final int MAX_TRIALS = 10;
    public static final int FEATURE_DIMENSION = 10;//52;//248;//10;/*6*/; //
    public static final int COMMON_FEATURE_DIMENSION = 10;
    public static final int MAX_NUMBER_OF_FEATURES = 11;/*7*/; //250
    public static final int TESTING_MAX_NUMBER_OF_ARMS = 10;//23;
    public static final HashMap<String, Integer> NUMBER_OF_DEVICES = new HashMap<String, Integer>();
    //public static int tmpPayoff=/*8*/ 16;
    public static int OptimumActionInputLocation = 10; //248;/*6*/
    //SQL Databases, Tables and Columns
    public static final String CONFIGGRAPH_ID = "configgraph_id";
    public static final String DB_ba = "response_set";
    public static final String CONFIGGRAPH_IS_NEW = "is_new";
    public static final String DB_Aa = "feature_set";
    public static final String CONFIGGRAPH_EXAMPLES = "example_count";
    public static final String CONFIGGRAPH_DETAIL = "graph_details";
    public static final String DB_Ba = "user_arm_matrix";

    public static final String DATABASE_NAME = /*"opportunity";*//*"car"*/ "artificial_data";//
    ;
    public static final String DATABASE_HOST_IP = "jdbc:mysql://172.26.191.189:3306/";//"jdbc:mysql://172.26.191.229:3306/";

    public static final String TABLE_NAME_CONTEXTINFO = /*"opportunity_contextinfo";*/ "contextinfo";/*  /*"opp_contextinfo_dul"*/
    ;
    public static final String TABLE_NAME_CONFIGGRAPH = /*"opportunity_configgraphs";*/ "configgraphs"; /* /* "opp_configgraphs_dul"*/
    ;
    public static final String TABLE_NAME_COMMONINFO = "commoninfo";
    public static final String TABLE_NAME_USERINFO = "userinfo";
    public static final String MAIN_COLUMN_NAME = "configgraph_id";

    public static final String COMMONINFO_FEATURE = "common_feature_mat";
    public static final String COMMONINFO_RESPONSE = "common_response_mat";
    public static final String COMMONINFO_BETA = "beta";
    public static final String COMMONINFO_ID = "id";

    //SQL Queries
    public static final String selectSQL = "SELECT " + CONFIGGRAPH_ID + "," + CONFIGGRAPH_IS_NEW + " FROM " + TABLE_NAME_CONFIGGRAPH;
    public static final String selectThreeSQL = "SELECT " + CONFIGGRAPH_ID + "," + CONFIGGRAPH_IS_NEW + "," + CONFIGGRAPH_EXAMPLES + " FROM " + TABLE_NAME_CONFIGGRAPH;

    public static final String selectAllSQL = "SELECT * FROM " + TABLE_NAME_CONTEXTINFO + " WHERE " + MAIN_COLUMN_NAME + " = ";
    public static final String countSQL = "SELECT COUNT(*) AS NumberOfExamples FROM " + TABLE_NAME_CONTEXTINFO + " WHERE " + MAIN_COLUMN_NAME + " = ";
    public static final String insertSQL = "INSERT INTO " + TABLE_NAME_CONTEXTINFO + " VALUES (";
    public static final String CONTEXTINFO_PAYOFF = "payoff";


    private static final HashMap<String, HashMap<String, Vector<String>>> userContextArm = new HashMap<String, HashMap<String, Vector<String>>>();
    private static final HashMap<String, ArrayList<double[]>> globalUserClustersDetails = new HashMap<String, ArrayList<double[]>>();

    /**
     * userContextArm maintains a dictionary of dictionaries where the key is the user cluster of the main dictionary.
     * The key on the second dictionary is the cluster of the general arm context and the value is the arm label.
     * getUserContextArm() reads data from a file and builds this hashmap and returns it.
     */

    public static HashMap<String, HashMap<String, Vector<String>>> getUserContextArm() {
        FileReader fileReader = null;
        try {
            // fileReader = new FileReader("D:\\intellij_projects\\Ocean-manual-data-manipulation\\build_the_tree_data.csv");
            fileReader = new FileReader("D:\\intellij_projects\\Ocean-manual-data-manipulation\\artificialdata_iot2016_build_the_tree_data.csv");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String input = bufferedReader.readLine();
            boolean isNotEnd = true;
            while (isNotEnd) {
                input = bufferedReader.readLine();
                if (input == null) {
                    isNotEnd = false;
                    break;
                }
                assert input != null;
                String[] array_input = input.split(","); //cluster0-0, user-1, arm-2
                if (userContextArm.keySet().contains(array_input[1])) {
                    if (userContextArm.get(array_input[1]).keySet().contains(array_input[0])) {
                        Vector<String> current_array = userContextArm.get(array_input[1]).get(array_input[0]);
                        current_array.add(array_input[2]);
                        userContextArm.get(array_input[1]).put(array_input[0], current_array);
                    } else {
                        // System.out.println(input);
                        Vector<String> new_array = new Vector<String>();
                        new_array.add(array_input[2]);
                        userContextArm.get(array_input[1]).put(array_input[0], new_array);
                    }
                } else {
                    HashMap<String, Vector<String>> tmp_arraylist = new HashMap<String, Vector<String>>();
                    Vector<String> tmp_string_array = new Vector<String>();
                    tmp_string_array.add(array_input[2]);
                    tmp_arraylist.put(array_input[0], tmp_string_array);
                    userContextArm.put(array_input[1], tmp_arraylist);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userContextArm;
    }

    public static HashMap<String, ArrayList<double[]>> getGlobalUserClusterDetailsFromFile(){
        FileReader fileReader= null;
        try {
            fileReader = new FileReader("D:\\intellij_projects\\Ocean-manual-data-manipulation\\artificialdata_iot2016_user_features.csv");


        BufferedReader bufferedReader=new BufferedReader(fileReader);
        String inputFeatureString=bufferedReader.readLine();
        while(inputFeatureString!=null){
            inputFeatureString=bufferedReader.readLine();
            if (inputFeatureString==null){
                break;
            }
            // System.out.println(inputFeatureString);
            String[] splitInputString=inputFeatureString.split(",");
            int lastIndex=splitInputString.length-1;
            final double[] parsedInput=new double[lastIndex];
            for (int i=0;i<lastIndex;i++){
                parsedInput[i]= Double.parseDouble(splitInputString[i]);
            }
            if(globalUserClustersDetails.keySet().contains(splitInputString[lastIndex])){
                globalUserClustersDetails.get(splitInputString[lastIndex]).add(parsedInput);
            }else{
                globalUserClustersDetails.put(splitInputString[lastIndex], new ArrayList<double[]>() {{add(parsedInput);}});
            }
        }

            for (String key:globalUserClustersDetails.keySet()){
                System.out.println(key+" "+globalUserClustersDetails.get(key).size());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return globalUserClustersDetails;


    }

    private static HashMap<Double, double[]> altRewardMap = new HashMap<Double, double[]>();

    public static HashMap<Double, double[]> getAltRewardMapForIoT() {
        altRewardMap.put(1.0, new double[]{7, 3});
        altRewardMap.put(2.0, new double[]{8});
        altRewardMap.put(3.0, new double[]{1, 9});
        altRewardMap.put(4.0, new double[]{6});
        altRewardMap.put(5.0, new double[]{5});
        altRewardMap.put(6.0, new double[]{4, 10});
        altRewardMap.put(7.0, new double[]{1, 9});
        altRewardMap.put(8.0, new double[]{2});
        altRewardMap.put(9.0, new double[]{7, 3});
        altRewardMap.put(10.0, new double[]{6});
        return altRewardMap;
    }


    public static HashMap<String, ArrayList<double[]>> getGlobalIoTUserClusterDetailsFromFile(String path){
        HashMap<String, ArrayList<double[]>> globalIotUserClusterDetails = new HashMap<String, ArrayList<double[]>>();
        FileReader fileReader= null;
        try {
            fileReader = new FileReader(path);

            BufferedReader bufferedReader=new BufferedReader(fileReader);
            String inputFeatureString=bufferedReader.readLine();
            while(inputFeatureString!=null){
                inputFeatureString=bufferedReader.readLine();
                if (inputFeatureString==null){
                    break;
                }
                // System.out.println(inputFeatureString);
                String[] splitInputString=inputFeatureString.split(",");
                int lastIndex=splitInputString.length-1;
                final double[] parsedInput=new double[lastIndex];
                for (int i=0;i<lastIndex;i++){
                    parsedInput[i]= Double.parseDouble(splitInputString[i]);
                }
                boolean addValue = true;
                if(globalIotUserClusterDetails.keySet().contains(splitInputString[lastIndex])){
                    ArrayList<double[]> currentList = globalIotUserClusterDetails.get(splitInputString[lastIndex]);
                    for (double[] valuseList: currentList){
                        if (valuseList[0]==parsedInput[0]){
                            addValue= false;
                        }
                    }

                    if (addValue){
                        globalIotUserClusterDetails.get(splitInputString[lastIndex]).add(parsedInput);
                    }

                }else{
                    globalIotUserClusterDetails.put(splitInputString[lastIndex], new ArrayList<double[]>() {{add(parsedInput);}});
                }
            }

            /*for (String key:globalIotUserClusterDetails.keySet()){
                System.out.println(key+" "+globalIotUserClusterDetails.get(key).size());
                for (double[] x: globalIotUserClusterDetails.get(key)){
                    for (double y: x){
                        System.out.print(y+",");
                    }
                    System.out.println("\n");
                }
            }*/

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return globalIotUserClusterDetails;


    }

    private static double[] containOnePr = new double[
            ]{0.005034, 0.129072, 0.003856, 0.000007, 0.000002, 0.033533, 0.044596, 0.203383, 0.046355, 0.000043,
            0.999860, 0.999926, 0.999539, 0.961618, 0.037184, 0.122528, 0.052013, 0.028371, 0.082964, 0.010969, 0.961868,
            0.999194, 0.002948, 0.002221, 0.000547, 0.999978, 0.978374, 0.885122, 0.996451, 0.990876, 0.999649, 0.000513,
            0.981337, 0.987734, 0.021301, 0.002028,};
    public static double getContainerOnePr(double armID){
        int index = (int) (armID -1);
        return containOnePr[index];
    }

    private static double[] containerTwoPr = new double[
            ]{0.994966 ,0.870928 ,0.996144, 0.999993, 0.999998, 0.966467, 0.955404, 0.796617, 0.953645, 0.999957,
            0.000140, 0.000074, 0.000461, 0.038382, 0.962816, 0.877472, 0.947987, 0.971629, 0.917036, 0.989031,
            0.038132, 0.000806, 0.997052, 0.997779, 0.999453, 0.000022, 0.021626, 0.114878, 0.003549, 0.009124,
            0.000351, 0.999487, 0.018663, 0.012266, 0.978699, 0.997972};

    public static double getContainerTwoPr(double armID){
        int index = (int)(armID-1);
        return containerTwoPr[index];
    }

    /*public static void main(String[] args){
        getGlobalUserClusterDetailsFromFile();
    }*/

    /*public static HashMap<Double,Double> sortByValues(HashMap<Double, Double> hmap){
        List list = new LinkedList(hmap.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        HashMap sortedHashMap = new LinkedHashMap();
        for (Object aList : list) {
            Map.Entry entry = (Map.Entry) aList;
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }*/
}





package org.oceanbandit.utils;

import java.util.ArrayList;

/**
 * Created by Nirandika Wanigasekara on 17/6/2017.
 */
public class ArrayConvertions {

    public double[] toPrimitiveDoubleArr(ArrayList<Double> arrL){
        int s = arrL.size();
        double[] d = new double[s];
        for (int i=0; i<s;i++){
            d[i]= arrL.get(i);
        }
        return d;
    }
}

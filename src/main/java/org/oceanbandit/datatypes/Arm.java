package org.oceanbandit.datatypes;

import java.util.Comparator;

/**
 * Created by Nirandika on 27/11/2014.
 * This class represents the arm of a multi-arm bandit problem. It has variables
 * to maintain the main feature of a typical arm in a bandit problem.
 * This class was built specially to run with the LinUCB algorith,
 */
public class Arm {
    private boolean isNew=true;
    private double[][] Aa; /*D*D matrix. Initially it will be the identity matrix. When training matrix is m+d, feature matrix is (m*d)(d+m)+I*/
    private double[] ba; /*D*1 matrix. Initially it will be all zero.*/
    private double[] theta; /*D*1 estimated theta value*/
    private int numberOfTrainingExamples; /*number of training examples for the arm*/
    private double armID; /*A unique id for the arm*/
    private String configuGraph;
    private double[][] Ba;
    private double payoff;
    private double totalScore;

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public double[][] getAa() {
        return Aa;
    }

    public void setAa(double[][] aa) {
        this.Aa = aa;
    }

    public double[] getba() {
        return ba;
    }

    public void setba(double[] ba) {
        this.ba = ba;
    }

    public double[] getTheta() {
        return theta;
    }

    public void setTheta(double[] theta) {
        this.theta = theta;
    }

    public int getNumberOfTrainingExamples() {
        return numberOfTrainingExamples;
    }

    public void setNumberOfTrainingExamples(int numberOfTrainingExamples) {
        this.numberOfTrainingExamples = numberOfTrainingExamples;
    }

    public double getArmID() {
        return armID;
    }

    public void setArmID(double armID) {
        this.armID = armID;
    }

    public String getConfiguGraph() {
        return configuGraph;
    }

    public void setConfiguGraph(String configuGraph) {
        this.configuGraph = configuGraph;
    }
    public double[][] getBa() {
        return Ba;
    }

    public void setBa(double[][] ba) {
        this.Ba = ba;
    }

    public double getPayoff() {
        return payoff;
    }

    public void setPayoff(double payoff) {
        this.payoff = payoff;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public Comparator<Arm> getPayOffComparator(){
        return new Comparator<Arm>() {
            public int compare(Arm o1, Arm o2) {
                return Double.compare(o1.getPayoff(), o2.getPayoff());
            }
        };
    }

    public Comparator<Arm> getTotalScoreComparator(){
        return new Comparator<Arm>() {
            public int compare(Arm o1, Arm o2) {
                return Double.compare(o1.getTotalScore(),o2.getTotalScore());
            }
        };
    }
}

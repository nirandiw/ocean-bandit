package org.oceanbandit.datatypes;

import Jama.Matrix;

/**
 * Created by Nirandika Wanigasekara on 24/2/2016.
 */
public class CommonArtifact {
    private Matrix A0; /*k*k matrix. Initially it will be the identity matrix. When training matrix is m*d, feature matrix is (m*d)(d+m)+I*/
    private Matrix b0; /*k*1 matrix. Initially it will be all zero.*/
    private Matrix beta; /*k*1 estimated beta value*/

    public CommonArtifact(int dimension){
        A0 =Matrix.identity(dimension,dimension);
        b0 =new Matrix(new double[dimension],dimension);
    }

    public Matrix getBeta() {
        return beta;
    }


    public void setBeta(double[] beta) {
        this.beta = new Matrix(beta, beta.length);
    }

    public Matrix getA0() {
        return A0;
    }

    public void setA0(Matrix a0) {

        this.A0 = a0;
    }

    public Matrix getb0() {

        return b0;
    }

    public void setb0(Matrix b0) {
        this.b0 = b0;
    }
}

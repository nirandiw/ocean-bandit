package org.oceanbandit.solver;

import org.oceanbandit.constantvalues.ConstantValues;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by workshop on 1/12/2014.
 */
public class TestRun {



    public static void main(String args[]) throws IOException {
        /*initialized for the LinUCB disjoint model*/
        //BanditSolver banditSolver=new BanditSolver("InformationGain.txt",ConstantValues.DATABASE_NAME, ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO, ConstantValues.MAX_NUMBER_OF_FEATURES,ConstantValues.FEATURE_DIMENSION, ConstantValues.ALPHA_T);

        /*initalized for the LinUCB hybrid model*/
        BanditSolver banditSolver=new BanditSolver("InformationGain.txt",ConstantValues.DATABASE_NAME, ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO, ConstantValues.MAX_NUMBER_OF_FEATURES,ConstantValues.FEATURE_DIMENSION, ConstantValues.COMMON_FEATURE_DIMENSION, ConstantValues.TABLE_NAME_USERINFO, ConstantValues.ALPHA_T, ConstantValues.NUMBER_OF_DEVICES);


        System.out.println("Selection option: 0 - no pre-processing , 1- only pre-processing, 2- pre-processing and LinUCB");
        Scanner scanner = new Scanner(System.in);
        String feedback = scanner.nextLine();
        if(feedback.equals("1")){
            banditSolver.preProcess();
        }else if (feedback.equals("0")){
            new TestRun().runBanditAlgorithm(banditSolver);
        }else if (feedback.equals("2")){
            banditSolver.preProcess();
            new TestRun().runBanditAlgorithm(banditSolver);
        }else{
            System.out.println("Enter a valid option.");
        }

    }

    public void runBanditAlgorithm(BanditSolver banditSolver) throws IOException { //TODO: Correct this properly for the proper data set
        Random random= new Random();
        double deployBucketNumberOfCorrectRecommendations=0;
        double deployBucketNumberOfTrials=0;
        int numberOfRecords=0;

        FileReader fileReader=new FileReader("newFeature.txt");
        BufferedReader bufferedReader=new BufferedReader(fileReader);
        double[] newFeature;//=new double[ConstantValues.FEATURE_DIMENSION];
        double[] newUserFeature;
        boolean isNotEndOfFile=true;
        double tot=1;

        File results= new File("results.txt");
        BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter(results));
//        double[] constantContext=banditSolver.artificialDataSimulator.getContextVector();
        while (isNotEndOfFile){
            int epsilon=random.nextInt(101);
            String inputFeatureString=bufferedReader.readLine();

            if (inputFeatureString!=null){
                numberOfRecords++;
                String[] splitUserArmFeatures=inputFeatureString.split("\\|");
                newFeature= /*banditSolver.artificialDataSimulator.getContextVector();*/ banditSolver.getDoubleArrayFromStringArray(splitUserArmFeatures[0], false, new double[ConstantValues.FEATURE_DIMENSION]);
                newUserFeature = banditSolver.getDoubleArrayFromStringArray(splitUserArmFeatures[1], true, new double[ConstantValues.COMMON_FEATURE_DIMENSION]);

                /*In order to do evaluations the traffic is split in to two buckets; Learning and deployment bucket.
                The learning bucket will run the bandit algorithm and give the recommendation. The deployment bucket
                 will give the already known recommendation.
                * */
                    int i=1;
                    while (i<=tot){
                        System.out.println("ROUND NUMBER=========== "+i);
                        //banditSolver.runLinUCBDisjointModel(newFeature,true); //for the disjoint model
                        try {
                            //banditSolver.runLinUCBHybridModel(newFeature, newUserFeature, true, splitUserArmFeatures[2], splitUserArmFeatures[3],/*"WHERE graph_details = \'monitor\'"*/""); //for the hybrid model
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.exit(-1);
                        }
                        System.out.println("======================================");
                            double learningPrecision = banditSolver.numberOfCorrectRecommendations / banditSolver.numberOfTrials;
                            double deploymentPrecision = deployBucketNumberOfCorrectRecommendations / deployBucketNumberOfTrials;
                         bufferedWriter.write("Learning Precision is: " + banditSolver.numberOfCorrectRecommendations + "/" + banditSolver.numberOfTrials + "=" + learningPrecision);
                            bufferedWriter.write("\n");
                        banditSolver.avgReward=0;
                        i++;
                   }
                bufferedWriter.write("New record \n");
            }else{
                isNotEndOfFile=false;
            }

        }
        bufferedWriter.write("Search time: "+banditSolver.timeSearch +" Update time: "+banditSolver.timeUpdate);
        bufferedWriter.write("Aplha = "+ ConstantValues.ALPHA_T+" ; Number of records "+numberOfRecords+" ; Each running for "+tot+" times.");

        bufferedReader.close();

        bufferedWriter.close();

    }


}

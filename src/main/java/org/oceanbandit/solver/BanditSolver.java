package org.oceanbandit.solver;

import Jama.Matrix;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.oceanbandit.constantvalues.ConstantValues;
import org.oceanbandit.datatypes.Arm;
import org.oceanbandit.datatypes.CommonArtifact;
import org.oceanbandit.utils.ArrayConvertions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Nirandika Wanigasekara on 27/11/2014.
 * This class is the LinUCB algorithm main functions and skeleton.
 * Provides methods to configure LinUCB in different ways.
 */
public class BanditSolver {


    private Logger LOGGER = Logger.getLogger(this.getClass().getName());
   private Logger TIME_LOGGER = Logger.getLogger(this.getClass().getName());
    private Random random = new Random();
    //Arm armWithMaxPayOff = null;
    int maxPayOffArmExamples = 0;

    ArrayList<Integer> extractedFeatures = new ArrayList<Integer>();
    ArrayList<Integer> extractedFeaturesUsers = new ArrayList<Integer>();
   // double maxPayOff = -1.0;
    double[] inputRecordArm;
    double[] inputRecordUser;
    double numberOfTrials = 0;
    double numberOfCorrectRecommendations = 0;
    double avgReward = 0;


    private String databaseName;
    private String tableNameConfigGraph;
    private String tableNameContextInfo;
    private String tableNameUserInfo;


    private double[] simulatedNewFeatureForArtificialFeedback;
    private double[] simulatedNewUserForArtificialFeedback;
    public ArtificialDataSimulator artificialDataSimulator = null;
    private HashMap<Double, double[]> simulatedThetaList = null;
    private double[] simulatedCommonBeta = null;
    public double accumilatedPayoff;
    private List<Arm> rankedList = null;
    private int featureDimensionMaxNumber;
    private int featureDimensionArm;


    public long timeSearch = 0;
    public long timeUpdate = 0;


    //hybrid model variables
    private CommonArtifact commonArtifact;
    private int featureDimensionCommon;

    private char rewardInfoLevel = 'B';


    //Important to set these up when I want different configurations for experiments.
    private static final boolean randomized = false; //when picking the arm randomly set this to true
    private static final boolean isArtificial = false; //when you want to generate the reward real time set to true
    private static final boolean isRewardInaccurate = false; //wheh generating a reward and it seens to have an error
    private static final boolean isRanked = true;

    private static HashMap<String, HashMap<String, Vector<String>>> userContextArmTree;// = new HashMap<String, HashMap<String, Vector<String>>>();

    private double localAlpha;


    int numberOfAdditionalFeedbackActions = 0;


    public BanditSolver(String intent, String databasename, String configgraphTableName, String contextInfoTableName,
                        int maxNumberOfFeatures, int armFeatureDimension, double alpha, HashMap<String, Integer> numberOfDevices) {
        getSignificantFeatures(intent); //TODO: Handle the case where featureDimention sensed is > significant features and also do a comparison.
        initializeMainGlobalVariables(databasename, configgraphTableName, contextInfoTableName, maxNumberOfFeatures,
                armFeatureDimension, alpha, numberOfDevices);
    }


    public BanditSolver(String intent, String databasename, String configgraphTableName, String contextInfoTableName,
                        int maxNumberOfFeatures, int armFeatureDimension, int commonFeatureDim, String userInfoTable,
                        double alpha, HashMap<String, Integer> numberOfDevices) {
        System.out.println("Bandit Solver Constructor");
        getSignificantFeatures(intent); //TODO: Handle the case where featureDimention sensed is > significant features and also do a comparison.
        initializeMainGlobalVariables(databasename, configgraphTableName, contextInfoTableName, maxNumberOfFeatures,
                armFeatureDimension, alpha,numberOfDevices);
        //parameters and methods for the LinUCB hybrid model
        this.tableNameUserInfo = userInfoTable;
        this.featureDimensionCommon = commonFeatureDim;
        inputRecordUser = new double[this.featureDimensionMaxNumber]; //TODO: Resolve this issue
        commonArtifact = new CommonArtifact(featureDimensionCommon);

        //setA0B0();
        if (isArtificial || rewardInfoLevel=='P'){
            //userContextArmTree = ConstantValues.getUserContextArm();

        }
    }

    public void setA0B0(Connection connection){
        LOGGER.log(Level.INFO,"setA0B0!!!!!");
        String selectSQL = "SELECT * from "+ ConstantValues.TABLE_NAME_COMMONINFO+ " WHERE ID = 1";
        ObjectMapper objectMapper= new ObjectMapper();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectSQL);
            while (resultSet.next()){
                    if (!resultSet.getString(ConstantValues.COMMONINFO_FEATURE).equals("") && !resultSet.getString(ConstantValues.COMMONINFO_RESPONSE).equals("") ){
                        double[][] tmpArray=objectMapper.readValue(resultSet.getString(ConstantValues.COMMONINFO_FEATURE), double[][].class);
                        commonArtifact.setA0(new Matrix(tmpArray));
                        commonArtifact.setb0(new Matrix(objectMapper.readValue(resultSet.getString(ConstantValues.COMMONINFO_RESPONSE), double[].class),featureDimensionCommon ));
                    }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setTimeLogger(){
        FileHandler fh;
        try {
            fh = new FileHandler("Time_Logger.log");

       // TIME_LOGGER.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initializeMainGlobalVariables(String databasename, String configgraphTableName, String contextInfoTableName, int maxNumberOfFeatures, int featureDimension, double alpha,HashMap<String, Integer> numberOfDevices) {
        this.databaseName = databasename;
        this.tableNameConfigGraph = configgraphTableName;
        this.tableNameContextInfo = contextInfoTableName;
        this.featureDimensionArm = featureDimension;// TODO: might have to set this based on getSignificantFeatures
        this.featureDimensionMaxNumber = maxNumberOfFeatures; //TODO:NOt good. Remove this
        inputRecordArm = new double[this.featureDimensionMaxNumber];
        localAlpha= alpha;
        System.out.println("Local alpha is"+localAlpha);
        System.out.println("Number of devices is "+numberOfDevices);

        if (numberOfDevices.containsKey("PARTIAL")){
            rewardInfoLevel='P';
            numberOfAdditionalFeedbackActions=numberOfDevices.get("PARTIAL");
        }else if (numberOfDevices.containsKey("FULL")){
            rewardInfoLevel='F';
        }else if (numberOfDevices.containsKey("BANDIT")){
            rewardInfoLevel='B';
        }else{
            rewardInfoLevel = 'C';
        }
        System.out.println("Reward Level is "+rewardInfoLevel);

        //setTimeLogger();

        if (isArtificial) {
            artificialDataSimulator = new ArtificialDataSimulator();
            simulatedThetaList = artificialDataSimulator.getUnitVectorSetForActions(10, 30); //Specify the number of actions you want to simulate
            simulatedCommonBeta = artificialDataSimulator.getCommonVector(10);
            for (double[] unitvec : simulatedThetaList.values()) {
                for (double x : unitvec) {
                    System.out.print(x + " ,");
                }
                System.out.println("");
            }
            accumilatedPayoff = 0;
        }
    }

    /**
     * This method will calculate all the feature vectors and response vectors for the arms known so far and save it in
     * the database. This pre-processing is done in order to avoid unnecessary calculations while running the LinUCB
     * algorithm. It will reduce the response time to the user for the recommendation.
     */
    public void preProcess() {
        Connection connection;
        Statement statement;
        ResultSet resultSet;


        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(ConstantValues.DATABASE_HOST_IP + databaseName, "ocean", "mit123");

            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT " + ConstantValues.CONFIGGRAPH_ID + "," + ConstantValues.CONFIGGRAPH_IS_NEW + " FROM " + tableNameConfigGraph);
            double isNew;
            double armID;
            while (resultSet.next()) {
                // Column two specifies if the config-graph is new or old. If it is new(1) no need to calculate the
                // feature set. If it is old(0) then we need to calculate the feature set.
                isNew = resultSet.getDouble(ConstantValues.CONFIGGRAPH_IS_NEW);
                if (isNew == 0) {
                    armID = resultSet.getDouble(ConstantValues.CONFIGGRAPH_ID);
                    //arm.setNumberOfTrainingExamples(numberOfExamples);
                    Arm initialArm = calculateInitialDesignMatrixes(armID, true, connection);//calculateArmFeatureSetResponseValues(armID, true/*, numberOfExamples*/, connection, null, -1);
                    writeArmToDataBase(initialArm, connection);
                    if (commonArtifact != null) {
                        calculateInitialUserArmFeatureSetValues(armID, true, initialArm, connection);
                    }
                }
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        LOGGER.log(Level.INFO, "Pre-processing completed successfully!");
    }

    /**
     * For different applications of the LinUCB (based on the type of arm you wish to evaluate) there will be different
     * features, and it is important to only consider the significant feature. In the current implementation we load
     * the significant features obtained from Weka software output file information gain.txt based on the
     * InfoGainAttributeEval Ranker method.
     *
     * @param intent
     */
    private int getSignificantFeatures(String intent) {
        if (intent.equals("InformationGain.txt")) {
            try {
                FileReader fileReader = new FileReader(intent);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                boolean notEndOfFile = true;
                int lineNumber = 1;
                while (notEndOfFile) {
                    String feature = bufferedReader.readLine();
                    if (feature == null) {
                        notEndOfFile = false;
                    } else {
                        if (lineNumber == 1) {
                            for (String value : feature.split(",")) {
                                extractedFeatures.add(Integer.parseInt(value));
                            }
                            lineNumber++;
                        } else if (lineNumber == 2) {
                            for (String value : feature.split(",")) {
                                extractedFeaturesUsers.add(Integer.parseInt(value));
                            }
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            extractedFeatures.add(3);
        }


        return extractedFeatures.size();
    }

    private Connection getDBConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(ConstantValues.DATABASE_HOST_IP + databaseName, "ocean", "mit123");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private Arm getArmValuesFromDB(double armID, /*int numberOfExamples,*/ double isNew, Connection connection) {
        long getArmValueStartTime= System.nanoTime();

        Arm arm = new Arm();
        double[][] Aa = null;
        double[][] Ba = null;
        double[] ba = null;

        ObjectMapper mapper = new ObjectMapper();
        try {
            arm.setArmID(armID);
            Statement contextinfoStmt = connection.createStatement();
            ResultSet contextinfoResults;
            if (isNew==1) {
                arm.setNew(true);

                contextinfoResults = contextinfoStmt.executeQuery("SELECT " + ConstantValues.CONFIGGRAPH_DETAIL
                        + " FROM " + tableNameConfigGraph + " WHERE " + ConstantValues.CONFIGGRAPH_ID + " = " + arm.getArmID());
                while (contextinfoResults.next()) {
                    arm.setConfiguGraph(contextinfoResults.getString(ConstantValues.CONFIGGRAPH_DETAIL));
                }
            }else {
                assert isNew == 0;
                arm.setNew(false);
                contextinfoResults = contextinfoStmt.executeQuery("SELECT " + ConstantValues.DB_Aa + ","
                        + ConstantValues.DB_ba + ", " + ConstantValues.DB_Ba + ", "+ConstantValues.CONFIGGRAPH_DETAIL
                        + " FROM " + tableNameConfigGraph + " WHERE " + ConstantValues.CONFIGGRAPH_ID + " = " + arm.getArmID());
                while (contextinfoResults.next()) {
                    Aa = mapper.readValue(contextinfoResults.getString(ConstantValues.DB_Aa), double[][].class); //TODO: check if this returns corrrect values
                    ba = mapper.readValue(contextinfoResults.getString(ConstantValues.DB_ba), double[].class);
                    if (!contextinfoResults.getString(ConstantValues.DB_Ba).equals("")) {
                        Ba = mapper.readValue(contextinfoResults.getString(ConstantValues.DB_Ba), double[][].class);
                    }
                    arm.setConfiguGraph(contextinfoResults.getString(ConstantValues.CONFIGGRAPH_DETAIL));
                }

                arm.setAa(Aa);
                arm.setba(ba);
                arm.setBa(Ba);

                contextinfoStmt.close();
                contextinfoResults.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long getArmValueEndTime= System.nanoTime();
        long getArmvalueDuration = getArmValueEndTime-getArmValueStartTime;
        TIME_LOGGER.log(Level.INFO, "Time to Populate Arm Values :"+String.valueOf(getArmvalueDuration));
        return arm;
    }

    public String runLinUCBDisjointModel(double[] newFeature, boolean isTesting) {
        //  LOGGER.log(Level.INFO, "runLinUCBDisjointModel");
        System.out.println("runLinUCBDisjointModel");
        setGlobalVariables(newFeature, null);
        Arm maxPayOffArm= new Arm();
        maxPayOffArm.setPayoff(Double.POSITIVE_INFINITY * -1);
        Connection connection = null;
        try {

            long startTime = System.nanoTime();
            String bestGraph = null;
            ResultSet resultSet;
            connection = getDBConnection();
            Statement statement = connection.createStatement();
            //ResultSet resultSet=statement.executeQuery("SELECT COUNT(*) AS MILLISEC FROM opportunity_contextinfo");
            resultSet = statement.executeQuery("SELECT " + ConstantValues.CONFIGGRAPH_ID + "," + ConstantValues.CONFIGGRAPH_IS_NEW + "," + ConstantValues.CONFIGGRAPH_EXAMPLES + " FROM " + tableNameConfigGraph);
            long startSelectionTime =System.nanoTime();
            while (resultSet.next()) {

                System.out.print("armID " + resultSet.getString(ConstantValues.CONFIGGRAPH_ID) + " "); //TODO: Remove
                double armID = resultSet.getDouble(ConstantValues.CONFIGGRAPH_ID);
                //int numberOfExamples = resultSet.getInt(ConstantValues.CONFIGGRAPH_EXAMPLES);
                double isNew = resultSet.getDouble(ConstantValues.CONFIGGRAPH_IS_NEW);
                if (armID != 0) { //TODO: why am i doing this check?

                    Arm arm = getArmValuesFromDB(armID, /*numberOfExamples,*/ isNew, connection);
                    LinucbDisjoint(getScaledFeature(newFeature), arm);
                    maxPayOffArm = findArmWithMaxPayoff(arm, maxPayOffArm);
                }
            }

            long endSelectionTime =System.nanoTime();
            long selection_duration = endSelectionTime-startSelectionTime;
            //TIME_LOGGER.log(Level.INFO, "Selection of an arm :"+String.valueOf(selection_duration));

            long endTime = System.nanoTime();
            //Implemented for testing purpose
            if (isTesting) {
                double reward = getUserFeedback(isTesting, maxPayOffArm.getArmID(), true);
                accumilatedPayoff = reward;
                System.out.println("Max payoff Arm is " + maxPayOffArm.getArmID() + " with pay off " + maxPayOffArm.getPayoff() + "and the reward is" + reward);
                Arm armw=writeNewFeatureToDataBase(reward, true, newFeature, maxPayOffArm.getArmID(), connection, false, null); //TODO Check if changing the 3rd parameter has an effect
                writeArmToDataBase(armw, connection);
                handleDifferentFeedbackLevels(true, connection, newFeature, maxPayOffArm);

            }
            bestGraph = maxPayOffArm.getArmID()+""; //getBestArmDetails(maxPayOffArm.getArmID(), connection);
            long duration = endTime - startTime;
            timeSearch += duration;
            System.out.println("Ocean-Bandit bestGraph " + bestGraph + "selectin time =" + duration);

            return bestGraph;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return "Unable to determine best graph";
    }

    public String runLinUCBHybridModel(double[] xta, HashMap<Double, ArrayList<Double>> newGlobalFeature,
                                       boolean isTesting, String task) throws Exception {

        LOGGER.log(Level.INFO,"runLinUCBHybridModel" );
        String filtering= getFilteringSQLCondition(task);
        //For each trial this method will be called
        setGlobalVariables(xta, new double[0]); //Todo: Check if isArtificial condition is needed
        Arm maxPayOffArm = new Arm();
        maxPayOffArm.setPayoff(Double.POSITIVE_INFINITY * -1);

        Connection connection;
        connection = getDBConnection();
        setA0B0(connection);
        //estimate the beta value.  Li et al., 2011] Algorithm 2 line 5
        Matrix A0 = commonArtifact.getA0();
        Matrix A0I = A0.inverse();
        Matrix beta = A0I.times(commonArtifact.getb0()); //TODO: Check if the calculation is correct

        //Li et al., 2011] Algorithm 2 line 6-15
        try {
            long startTime = System.nanoTime();
            String bestGraph;

            Statement statement = connection.createStatement();

            String sqlSelect = "SELECT " + ConstantValues.CONFIGGRAPH_ID + "," + ConstantValues.CONFIGGRAPH_IS_NEW + "," +
                    ConstantValues.CONFIGGRAPH_EXAMPLES + " FROM " + tableNameConfigGraph+" "+filtering;
            System.out.println(sqlSelect);
            ResultSet resultSet = statement.executeQuery(sqlSelect);
            long searchTimeStartHybrid = System.nanoTime();

            while (resultSet.next()) {

                System.out.print("armID " + resultSet.getString(ConstantValues.CONFIGGRAPH_ID) + " "); //TODO: Remove
                double armID = resultSet.getDouble(ConstantValues.CONFIGGRAPH_ID);
                double isNew = resultSet.getDouble(ConstantValues.CONFIGGRAPH_IS_NEW);
                if (armID != 0) { //TODO: why am i doing this check?

                    Arm arm = getArmValuesFromDB(armID, /*numberOfExamples,*/ isNew, connection);
                    double[] zta = getZta(newGlobalFeature.get(armID), xta);
                    double[] xta_scaled = getScaledFeature(xta);
                    double[] zta_scaled = getScaledFeature(zta);
                    LinucbHybrid(xta_scaled, zta_scaled, arm, beta);
                    maxPayOffArm = findArmWithMaxPayoff(arm, maxPayOffArm);

                }
            }
            long searchTimeEndHybrid =System.nanoTime();
            long searchTimeDurationHybrid = searchTimeEndHybrid-searchTimeStartHybrid;
            TIME_LOGGER.log(Level.INFO, "Search Time duration hybrid "+String.valueOf(searchTimeDurationHybrid));
            bestGraph = maxPayOffArm.getArmID()+""; //getBestArmDetails(maxPayOffArmID, connection);
            long endTime = System.nanoTime();

            /*if (isTesting) {
                double realValuedPayOff = getUserFeedback(true, maxPayOffArmID, true);
                accumilatedPayoff = realValuedPayOff;
                System.out.println("Arm with Max payoff is " + maxPayOffArmID + " with pay off " + maxPayOff + "and the realvaluedPayoff is" + realValuedPayOff);
                writeNewFeatureToDataBaseHybridModel(realValuedPayOff, isTesting, newArmFeature, newUserFeature, maxPayOffArmID, connection); //TODO: Check if changing the 3rd an 4th parameter has an effect
                handleDifferentFeedbackLevelsHybirdModel(isTesting, connection, user_cluster, context_cluster, newArmFeature, newUserFeature);

            }*/
            //bestGraph = "<id>" + maxPayOffArmID + "</id>" + bestGraph;
            connection.close();

            if (rewardInfoLevel=='P' &&  !isTesting) {
                String[] arrTasks = task.split(";");
                ArrayList<String> arrListTasks = new ArrayList<String>(Arrays.asList(arrTasks));
                if (arrListTasks.contains(maxPayOffArm.getConfiguGraph())){
                    arrListTasks.remove(maxPayOffArm.getConfiguGraph());
                }
                bestGraph = maxPayOffArm.getArmID()+",";
                rankedList.remove(maxPayOffArm);
                Collections.sort(rankedList, new  Arm().getPayOffComparator());
                int counter =0;
                for(Arm a: rankedList){
                    if (arrListTasks.contains(a.getConfiguGraph())){
                        bestGraph = bestGraph + a.getArmID() + ",";
                        counter++;
                        if (counter==numberOfAdditionalFeedbackActions){
                            break;
                        }
                        arrListTasks.remove(a.getConfiguGraph());
                    }
                }

            }else if (rewardInfoLevel == 'C'){
                ConComMSolver conComMSolver = new ConComMSolver(maxPayOffArm, rankedList, task.split(";"),databaseName,tableNameConfigGraph);
                bestGraph = conComMSolver.selectSmartCompositeService().toString().replace("[","").replace("]","");


            }else{
                bestGraph = maxPayOffArm.getArmID()+"";
            }
            long duration = endTime - startTime;
            timeSearch += duration;
            System.out.println("Ocean-Bandit bestGraph " + bestGraph + "selectin time =" + duration);
            return bestGraph;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return "Unable to determine best graph";
    }

    private double[] getZta(ArrayList<Double> armF, double[] userF){
       double[] arrArmF = new ArrayConvertions().toPrimitiveDoubleArr(armF);
        Matrix matArmF = new Matrix(arrArmF, arrArmF.length);
        Matrix matUserF = new Matrix(userF,1);
        Matrix g = matArmF.times(matUserF);
        double[] zta_new = g.getColumnPackedCopy();
        return zta_new;
    }

    private static String getFilteringSQLCondition(String intent) {
        String[] capabilities = intent.split(";");
        StringBuilder sqlFiltering = new StringBuilder("WHERE "+ConstantValues.CONFIGGRAPH_DETAIL+" = \'");
        sqlFiltering.append(String.join("\' OR "+ConstantValues.CONFIGGRAPH_DETAIL+" = \'", capabilities));
        sqlFiltering.append("\'");
        return sqlFiltering.toString();
    }

    private void handleDifferentFeedbackLevels(boolean isTesting, Connection connection, double[] inputRecordArm, Arm maxPayOffArm) throws SQLException {
        double tmpRewardValue;
        switch (rewardInfoLevel) {
            case 'F':
                System.out.println("Reward Full Information ");
                for (double j = 1; j < ConstantValues.TESTING_MAX_NUMBER_OF_ARMS + 1; j++) {
                    tmpRewardValue = getUserFeedback(true, j, false);
                    if (isArtificial && j != maxPayOffArm.getArmID()) {
                        Arm armWrite = writeNewFeatureToDataBase(getUserFeedback(true, j, false), isTesting, inputRecordArm, j, connection, false, null);
                        writeArmToDataBase(armWrite, connection);

                    } else if (/*realValuedPayOff==1 &&*/ j != maxPayOffArm.getArmID()) {
                        Arm armWrite=writeNewFeatureToDataBase(tmpRewardValue, isTesting, inputRecordArm, j, connection, false, null);
                        writeArmToDataBase(armWrite, connection);

                    }
                }
                break;
            case 'B':
                System.out.println("Reward Bandit Level");
                break;
            case 'P':
                System.out.println("Reward Partial level");
                int numberOfAdditionalFeedbackActions = 3;
                //rankedList.remove(maxPayOffArmID);
                long partial_feedback_start = System.nanoTime();
                handleBanditPartialFeedback(connection, numberOfAdditionalFeedbackActions, inputRecordArm);
                long partual_feedback_end =System.nanoTime();
                long partila_feedback_duration = partual_feedback_end-partial_feedback_start;
                TIME_LOGGER.log(Level.INFO, "Partial Feedback:  ", partila_feedback_duration);
                break;
            default:
                System.out.println("Reward Infomation Level NOT set to a valid values");
                break;
        }
    }

    /*private void handleDifferentFeedbackLevelsHybirdModel(boolean isTesting, Connection connection, String user_cluster, String context_cluster, double[] newFeature, double[] newUser, Arm maxPayOffArm) throws SQLException {
        double tmpRewardValue;
        switch (rewardInfoLevel) {
            case 'F':
                System.out.println("Reward Full Information ");
                for (double j = 1; j < ConstantValues.TESTING_MAX_NUMBER_OF_ARMS + 1; j++) {
                    tmpRewardValue = getUserFeedback(isTesting, j, false);
                    if (tmpRewardValue == 0 && commonArtifact != null) { //tries to capture that its the hybrid model
                        tmpRewardValue = getAlterRewardForAltrArm(user_cluster, context_cluster, j);
                    }
                    System.out.println("User Cluster Details "+userClustersDetails.size());

                    double[] simillarUser = getSecondaryUserInformation(user_cluster,userClustersDetails);
                    Connection tmpConnection= getDBConnection();
                    if (isArtificial && j != maxPayOffArm.getArmID()) {

                        writeNewFeatureToDataBaseHybridModel(tmpRewardValue, isTesting, newFeature, simillarUser, j, tmpConnection);
                    } else if (/*realValuedPayOff==1 &&*/ /*j != maxPayOffArm.getArmID()) {
                        writeNewFeatureToDataBaseHybridModel(tmpRewardValue, isTesting, newFeature, simillarUser, j, tmpConnection);
                    }
                }
                break;
            case 'B':
                System.out.println("Reward Bandit Level");
                break;
            case 'P':
                System.out.println("Reward Partial level");
                //rankedList.remove(maxPayOffArmID);
                handleBanditPartialFeedbackHybridModel(connection, numberOfAdditionalFeedbackActions, user_cluster, context_cluster, newFeature);
                break;
            default:
                System.out.println("Reward Infomation Level NOT set to a valid values");
                break;
        }
    }*/

    private double getAlterRewardForAltrArm(String user_cluster, String context_cluster, double j) {
        double feedback = 0.1;
        System.out.println("User Cluster "+user_cluster.trim()+" . Context Cluster "+context_cluster.trim());
        Vector<String> array_similar_arms = userContextArmTree.get(user_cluster.trim()).get(context_cluster.trim());
        /*if (array_similar_arms==null){
            return feedback;
        }*/
        for (String similar_arm : array_similar_arms) {
            if (Double.parseDouble(similar_arm) == j) {
                feedback = 1.0;
                LOGGER.log(Level.INFO, "Alternate Feedback MATCHED. Reward is 1");
                break;
            }
        }
        return feedback;

    }

    private void handleBanditPartialFeedback(Connection connection, int numberOfAdditionalFeedbackActions, double[] newFeature) throws SQLException {
        Collections.sort(rankedList, new Arm().getPayOffComparator());
        List<Arm> maxAltArms = rankedList.subList(1,numberOfAdditionalFeedbackActions);
        for (Arm maxAltArm: maxAltArms) {
            //get the true reward of the alternative maximum payoff arm
            double trueRewardForMaxAltArm = getUserFeedback(true, maxAltArm.getArmID(), false);

            Arm armWrite=writeNewFeatureToDataBase(trueRewardForMaxAltArm, true, newFeature, maxAltArm.getArmID(), connection, false, null);
            writeArmToDataBase(armWrite, connection);
            rankedList.remove(maxAltArm);
        }
    }

    /*private void handleBanditPartialFeedbackHybridModel(Connection connection, int numberOfAdditionalFeedbackActions, String user_cluster, String context_cluster, double[] newArm) throws SQLException {

        Collections.sort(rankedList, new Arm().getPayOffComparator());

        List<Arm> partialArms = rankedList.subList(1,numberOfAdditionalFeedbackActions);

        for (Arm maxAltArm: partialArms){
            double trueRewardForMaxAltArm = getUserFeedback(true, maxAltArm.getArmID(), false);//getAltrRewardForIoTSetting(maxAltArm);//getAlterRewardForAltrArm(user_cluster, context_cluster, maxAltArm);
            double[] similarUser = getSecondaryUserInformation(user_cluster, userClustersDetails);
            double[] similarArm = getSimillarArmForIoTSetting(context_cluster, contextclusterDetails);
            Connection tmpConnection = getDBConnection();
            writeNewFeatureToDataBaseHybridModel(trueRewardForMaxAltArm, true, similarArm, similarUser, maxAltArm.getArmID(), tmpConnection);
        }


    }*/

    private double[] getSimillarArmForIoTSetting(String context_cluster_number, HashMap<String, ArrayList<double[]>> contextclusterDetails) {
        System.out.println("getSecondaryUserInformation: cluster number "+context_cluster_number+ " cluster details "+contextclusterDetails.size());
        ArrayList<double[]> tmpArray=contextclusterDetails.get(context_cluster_number);
        int index = random.nextInt(tmpArray.size());
        System.out.println("Index " + index);
        double[] altContext = tmpArray.get(index);
        return altContext;
    }

    private String getBestArmDetails(double tmpArmID, Connection connection) throws SQLException {

        Statement getBestGrpah;
        ResultSet bestGraphResults;
        String bestGraph = "";
        getBestGrpah = connection.createStatement();
        bestGraphResults = getBestGrpah.executeQuery("SELECT " + ConstantValues.CONFIGGRAPH_DETAIL + " FROM " + tableNameConfigGraph + " WHERE " + ConstantValues.CONFIGGRAPH_ID + " = " + tmpArmID);
        while (bestGraphResults.next()) {
            bestGraph = bestGraphResults.getString(ConstantValues.CONFIGGRAPH_DETAIL);
        }
        return bestGraph;
    }

    /**
     * A method used to set values for different variables based on the configuration needed for the algorithm
     */
    private void setGlobalVariables(double[] newArmFeature, double[] newUserFeatures) {
        numberOfTrials++;
        if (isArtificial) {
            simulatedNewFeatureForArtificialFeedback = newArmFeature; //TODO:Check if this is the correct place.
            simulatedNewUserForArtificialFeedback = newUserFeatures;
            inputRecordArm = newArmFeature;
            inputRecordUser = newUserFeatures;
        }

        if (isRanked) {
            rankedList = new ArrayList<Arm>();
        }
    }

    public void writeNewFeatureToDataBaseHybridModel(double realValuedPayOff, boolean isTesting, double[] xta, double[] zta,
                                                     double maxPayOffArmID, Connection connection) throws SQLException {
        System.out.println("Feedback for arm  "+ maxPayOffArmID);
        Arm maxPayoffArmObj = getMaxPayoffArmDetails(maxPayOffArmID, connection);
        if (maxPayoffArmObj==null){
            LOGGER.log(Level.WARNING,"Arm is null "+ maxPayOffArmID);
            System.exit(-1);
        }
        long startTime = System.nanoTime();
        if (!isTesting){
            System.out.println("Not testing need to set common aritifact values from DB");
            setA0B0(connection);
        }else{
            System.out.println("Is testing No need to set common aritifact values from DB");
        }
        CommonArtifact tmpCommonArtifact = updateCommonArtifactOnce(maxPayoffArmObj);  //Li et al., 2011] Algorithm 2 line 17-18
        double[] xta_scaled = getScaledFeature(xta);
        double[] zta_scaled =  getScaledFeature(zta);
        try {
            maxPayoffArmObj = writeNewFeatureToDataBase(realValuedPayOff, isTesting, xta_scaled, maxPayOffArmID, connection, true, zta_scaled);  //Li et al., 2011] Algorithm 2 line 19, 21
            writeArmToDataBase(maxPayoffArmObj, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        updateCommonArtifactLast(maxPayoffArmObj, connection, tmpCommonArtifact, zta_scaled,realValuedPayOff); //Line 22-23 LunICB-Hybrid
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        TIME_LOGGER.log(Level.INFO, "Update duration in LinUCB Hybrid" + duration);
        connection.close();
    }

    private Arm calculateInitialUserArmFeatureSetValues(double armId, boolean isTesting, Arm arm, Connection connection) throws SQLException {
        int numberOfExamplesArm = getNumberOfExamples(armId, tableNameContextInfo, connection);
        int numberOfExamplesUserArm = getNumberOfExamples(armId, tableNameUserInfo, connection);

        if (numberOfExamplesArm != numberOfExamplesUserArm) {
            System.exit(-1);
        } else {
            System.out.println("Number of  Training Examples for arm " + armId + " = " + numberOfExamplesArm + " users =" + numberOfExamplesUserArm);
            arm.setNumberOfTrainingExamples(numberOfExamplesArm);
            if (numberOfExamplesArm > 0) {
                arm.setNew(false);
                arm.setNumberOfTrainingExamples(numberOfExamplesArm);
            }
        }
        double[][] armTrainingExamples = new double[numberOfExamplesArm][featureDimensionArm];
        double[][] userTrainingExamples = new double[numberOfExamplesUserArm][featureDimensionCommon];

        prepareDataMatrixes(armId, isTesting, armTrainingExamples, null, arm, extractedFeatures, tableNameContextInfo, connection);
        prepareDataMatrixes(armId, isTesting, userTrainingExamples, null, arm, extractedFeaturesUsers, tableNameUserInfo, connection);

        Matrix matArmTrainingEx = new Matrix(armTrainingExamples);
        Matrix matUserTrainingEx = new Matrix(userTrainingExamples);
        double[][] userArmMatrix = matArmTrainingEx.transpose().times(matUserTrainingEx).getArray(); //TODO:Check if the calculation is write
        arm.setBa(userArmMatrix);
        writeArmToDataBase(arm, connection);
        return arm;

    }

    private Arm calculateArmFeatureSetResponseValues(double armID, Connection connection, double[] xta,
                                                     double reward, boolean isHybrid, double[] zta) throws SQLException { //TODO:Figre out a better way to handle the number of examples.
        long startTime = System.nanoTime();
        String select_query = "SELECT * FROM "+tableNameConfigGraph+" WHERE "+ConstantValues.CONFIGGRAPH_ID+" = "+armID;
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(select_query);
        Matrix mat_Aa = null;
        Matrix mat_ba = null;
        Matrix mat_Ba = null;
        while (resultSet.next()){
            if (resultSet.getString(ConstantValues.CONFIGGRAPH_IS_NEW).equals("1")){
                mat_Aa = Matrix.identity(featureDimensionArm,featureDimensionArm);
                mat_ba = new Matrix(new double[featureDimensionArm], featureDimensionArm);
                if (isHybrid){
                    mat_Ba = new Matrix(featureDimensionArm,featureDimensionCommon);
                }
            }else {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    mat_Aa = new Matrix(mapper.readValue(resultSet.getString(ConstantValues.DB_Aa), double[][].class));
                    mat_ba = new Matrix(mapper.readValue(resultSet.getString(ConstantValues.DB_ba), double[].class), featureDimensionArm);
                    if (isHybrid){
                        mat_Ba = new Matrix(mapper.readValue(resultSet.getString(ConstantValues.DB_Ba), double[][].class));
                        assert mat_Ba !=null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        assert mat_Aa != null;
        assert mat_ba != null;



        Matrix xta_mat = new Matrix(xta, xta.length);
        mat_Aa.plusEquals(xta_mat.times(xta_mat.transpose()));

        mat_ba.plusEquals(xta_mat.times(reward));
        Arm arm = new Arm();
        arm.setAa(mat_Aa.getArray());
        arm.setba(mat_ba.getColumnPackedCopy());

        if (isHybrid && mat_Ba !=null){
            Matrix matNewUser = new Matrix(zta, zta.length);
            mat_Ba.plusEquals(xta_mat.times(matNewUser.transpose()));
            arm.setBa(mat_Ba.getArray());
        }

        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        TIME_LOGGER.log(Level.INFO, "Update time in a iteration Disjoint part :"+String.valueOf(duration));
        timeUpdate += duration;

        arm.setArmID(armID);
        arm.setNew(false);
        return arm;

    }

    private Arm calculateInitialDesignMatrixes(double armID, boolean isTesting, Connection connection) throws SQLException {

        double[][] trainingExamples;
        double[] userFeedback;
        double[][] armFeatureSet;
        Arm arm = new Arm();
        // Connection connection = getDBConnection();

        //Count the number of examples recorded for this configGraph for it is needed to declare
        // the feature matrix
        int numberOfExamples = getNumberOfExamples(armID, tableNameContextInfo, connection);

        System.out.println("Number of  Training Examples for arm " + armID + " = " + numberOfExamples);
        arm.setNumberOfTrainingExamples(numberOfExamples);
        if (numberOfExamples > 0) {
            arm.setNew(false);
        }
        //Declare the trainingExamaples array and the userfeedback arraybased on the size of the examples for the arm
        trainingExamples = new double[numberOfExamples][featureDimensionArm];
        userFeedback = new double[numberOfExamples];

        //Now populate the feature matrix and the response matrix for each example recorded for the current configGraph
        prepareDataMatrixes(armID, isTesting, trainingExamples, userFeedback, arm, extractedFeatures, tableNameContextInfo, connection);




        Matrix matTrainingExamples = new Matrix(trainingExamples);
        // matTrainingExamples.print(5000,17);
        //assert matTrainingExamples != null;
        Matrix matTransponseTrainingEx = matTrainingExamples.transpose();
        armFeatureSet = (matTransponseTrainingEx.times(matTrainingExamples)).plus(Matrix.identity(featureDimensionArm, featureDimensionArm)).getArray();

        arm.setAa(armFeatureSet);
        //Calcuate the response vector for the arm
        double[] responseVector = new double[featureDimensionArm];//new double[transposeOfTrainingExamples.length];
        if (userFeedback != null) { //The responseVector will not have any values if the user OptimumActionInputLocation is null.This means the arm is new. In that case LinUCB() should be able to handle it.
            Matrix matUserFeedback = new Matrix(userFeedback, userFeedback.length);
            responseVector = matTransponseTrainingEx.times(matUserFeedback).getColumnPackedCopy();
        }
        arm.setba(responseVector);
        return arm;
    }

    private CommonArtifact updateCommonArtifactOnce(Arm expectedBestArm) {
        // Temparary adjustment. So should not effect other calculations.
        CommonArtifact artifact= new CommonArtifact(ConstantValues.COMMON_FEATURE_DIMENSION);
        Matrix mat_Aa = new Matrix(expectedBestArm.getAa());
        Matrix mat_Ba = new Matrix(expectedBestArm.getBa());
        Matrix mat_temp = mat_Ba.transpose().times(mat_Aa.inverse()).times(mat_Ba);

        Matrix A0_tmp = commonArtifact.getA0().plus(mat_temp); //line 17 LinUCB-Hybrid
        artifact.setA0(A0_tmp);

        Matrix mat_ba = new Matrix(expectedBestArm.getba(), expectedBestArm.getba().length);
        mat_temp = mat_Ba.transpose().times(mat_Aa.inverse()).times(mat_ba);
        Matrix b0_tmp = commonArtifact.getb0().plus(mat_temp); //line 18 LinUCB-Hybrid
        artifact.setb0(b0_tmp);

        assert(artifact.getA0().getRowDimension() == ConstantValues.COMMON_FEATURE_DIMENSION &&
                artifact.getA0().getColumnDimension() == ConstantValues.COMMON_FEATURE_DIMENSION);

        assert(artifact.getb0().getRowDimension()==ConstantValues.COMMON_FEATURE_DIMENSION);

        return artifact;
    }

    private void updateCommonArtifactLast(Arm maxPayoffArmObj, Connection connection, CommonArtifact tmpArtifact,
                                          double[] zta_s, double r) throws SQLException { //TODO:SOmething's really wrong with the tmp calculation


        Matrix Aa_mat = new Matrix(maxPayoffArmObj.getAa());
        Matrix Ba_mat = new Matrix(maxPayoffArmObj.getBa());

        Matrix zta_mat = new Matrix(zta_s, zta_s.length);
        Matrix userTerm=zta_mat.times(zta_mat.transpose());

        tmpArtifact.getA0().plusEquals(userTerm);

        Matrix A0_final = tmpArtifact.getA0().minus(Ba_mat.transpose().times(Aa_mat.inverse().times(Ba_mat))); //Line 22 - LinUCB-H

        Matrix ba_mat = new Matrix(maxPayoffArmObj.getba(), maxPayoffArmObj.getba().length);

        Matrix b0_final = tmpArtifact.getb0().plus(zta_mat.times(r)).minus(Ba_mat.transpose().times(Aa_mat.inverse().times(ba_mat)));

        commonArtifact.setb0(b0_final);

        commonArtifact.setA0(A0_final);
        writeCommonArtifactToDatabase(commonArtifact, connection); //TODO: Do it lessurely
        connection.close();

    }

    private void writeCommonArtifactToDatabase(CommonArtifact commonArtifact, Connection connection) throws SQLException {
        //Connection connection = getDBConnection();
        Statement statmentInsertCommonArtifat = connection.createStatement();
        String strCommonFeature = getAsJSON(commonArtifact.getA0().getArray());
        String strCommonResponse = getAsJSON(commonArtifact.getb0().getColumnPackedCopy());
        String strBeta = getAsJSON(commonArtifact.getBeta());
        /*INSERT INTO " + ConstantValues.TABLE_NAME_COMMONINFO + " VALUES (1, '" + strCommonFeature + "','" + strCommonResponse + "','" + strBeta + "')" +
                "ON DUPLICATE KEY*/
        String insertSQL = " UPDATE "+ ConstantValues.TABLE_NAME_COMMONINFO+" SET " + ConstantValues.COMMONINFO_FEATURE + "='" + strCommonFeature + "', " +
                ConstantValues.COMMONINFO_RESPONSE + "='" + strCommonResponse + "'," +
                ConstantValues.COMMONINFO_BETA + " = " + "'" + strBeta + "' WHERE "+ConstantValues.COMMONINFO_ID+" = 1";
        // System.out.println(insertSQL);
        int insertIsSuccess = statmentInsertCommonArtifat.executeUpdate(insertSQL);
        if (insertIsSuccess < 0) {
            System.out.println("Error Saving Common Artificat Details");
            System.exit(-1);
        }
        // connection.close();
    }

    private Arm getMaxPayoffArmDetails(double armid, Connection connection) {
        //Connection connection = getDBConnection();
        String sqlSELECT = "SELECT * FROM " + tableNameConfigGraph + " WHERE " + ConstantValues.CONFIGGRAPH_ID + " =" + armid;
        Arm tmpArm = new Arm();
        tmpArm.setArmID(armid);
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSetArmDetails = statement.executeQuery(sqlSELECT);
            while (resultSetArmDetails.next()) {
                double isNew = resultSetArmDetails.getDouble(ConstantValues.CONFIGGRAPH_IS_NEW);

                if (isNew == 1) {
                    System.out.println("Is a new ARM");
                    tmpArm.setAa(getIdentityMatrix(featureDimensionArm));
                    tmpArm.setba(new double[featureDimensionArm]);
                    tmpArm.setBa(new double[featureDimensionArm][featureDimensionCommon]);
                } else {
                    System.out.println("Is not a new arm");
                    tmpArm.setAa(objectMapper.readValue(resultSetArmDetails.getString(ConstantValues.DB_Aa), double[][].class));
                    tmpArm.setBa(objectMapper.readValue(resultSetArmDetails.getString(ConstantValues.DB_Ba), double[][].class));
                    tmpArm.setba(objectMapper.readValue(resultSetArmDetails.getString(ConstantValues.DB_ba), double[].class));
                }


            }
            statement.close();
            resultSetArmDetails.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tmpArm;
    }

    private double calculateUCBHybrid(double[] xta_t, double[] zta_t, Matrix Aa_t, double[][] Ba_t, Matrix A0_t) {
        double ucbHybrid;
        double term1;
        double term2;
        double term3;
        double term4;


        Matrix mat_xta = new Matrix(xta_t, xta_t.length);
        Matrix mat_zta = new Matrix(zta_t, zta_t.length);

        Matrix mat_Ba = new Matrix(Ba_t);

        Matrix matTerm1 = mat_zta.transpose().times(A0_t.inverse()).times(mat_zta);
        assert(matTerm1.getColumnDimension() ==1 && matTerm1.getRowDimension() ==1);
        term1 = matTerm1.get(0,0);

        Matrix matTerm2 = mat_zta.transpose().times(-2).times(A0_t.inverse()).
                times(mat_Ba.transpose()).times(Aa_t.inverse()).times(mat_xta);
        assert(matTerm2.getColumnDimension() ==1 && matTerm2.getRowDimension() ==1);
        term2 = matTerm2.get(0, 0);

        Matrix matTerm3 = mat_xta.transpose().times(Aa_t.inverse()).times(mat_xta);
        assert(matTerm3.getColumnDimension() ==1 && matTerm3.getRowDimension() ==1);
        term3 = matTerm3.get(0, 0);


        Matrix matTerm4 = mat_xta.transpose().times(Aa_t.inverse()).times(mat_Ba).
                times(A0_t.inverse()).times(mat_Ba.transpose()).times(Aa_t.inverse()).times(mat_xta);
        assert(matTerm4.getColumnDimension() ==1 && matTerm4.getRowDimension() ==1);
        term4 = matTerm4.get(0, 0);

        System.out.println("Term 1 "+term1+ " "+"Term 2 "+term2 +" "+"Term 3 "+term3+"Term 4 "+term4+" ");

        ucbHybrid = term1 + term2 + term3 + term4;

        System.out.println("UCB "+ucbHybrid);
        return ucbHybrid;
    }

    private double calculateUCB(double[] _xta, Matrix AaI) {
        Matrix _xta_mat= new Matrix(_xta, _xta.length);
        Matrix tmp = _xta_mat.transpose().times(AaI).times(_xta_mat);

        assert(tmp.getRowDimension() ==1 && tmp.getColumnDimension() == 1);

        double tmpUCB=tmp.get(0,0);
        tmpUCB = Math.sqrt(tmpUCB);
        double UCB = localAlpha * tmpUCB;
        System.out.println( "UCB "+UCB);
        return UCB;
    }

    private Matrix get_baMinusBaTimeBeta(Arm arm, Matrix matBeta) {
        Matrix matBa = new Matrix(arm.getBa());

        Matrix matba = new Matrix(arm.getba(), arm.getba().length);

        Matrix adjustmentOne = matBa.times(matBeta);
        return matba.minus(adjustmentOne);

    }

    private int getNumberOfExamples(double armID, String tableName, Connection connection) throws SQLException {
        //Connection connection = getDBConnection();
        Statement statementCountExamples = connection.createStatement();
        String countSQL = "";
        if (armID != 0) {
            countSQL = "SELECT COUNT(*) AS NumberOfExamples FROM " + tableName + " WHERE " + ConstantValues.CONFIGGRAPH_ID + " = " + armID;
        } else {
            countSQL = "SELECT COUNT(*) AS NumberOfExamples FROM " + tableName;
        }

        System.out.println(countSQL);
        ResultSet numberOfExampleResultSet = statementCountExamples.executeQuery(countSQL);
        int numberOfExamples = 0;
        while (numberOfExampleResultSet.next()) {
            numberOfExamples = numberOfExampleResultSet.getInt("NumberOfExamples");
        }

        statementCountExamples.close();
        //connection.close();
        return numberOfExamples;

    }

    private void prepareDataMatrixes(double armID, boolean isTesting, double[][] trainingExamples, double[] userFeedback, Arm arm, ArrayList<Integer> extractedFeaturesLocal, String tableName, Connection connection) throws SQLException {
        //Connection connection = getDBConnection();
        Statement statementFeatureSet;
        ResultSet featureSet;
        statementFeatureSet = connection.createStatement();
        String sqlQuery = null;
        if (arm != null) {
            sqlQuery = "SELECT * FROM " + tableName + " WHERE " + ConstantValues.CONFIGGRAPH_ID + " = " + armID;
        } else {
            sqlQuery = "SELECT * FROM " + tableName;
            arm = new Arm();
        }

        featureSet = statementFeatureSet.executeQuery(sqlQuery);

        arm.setArmID(armID);
        int exampleNumber = 0;
        while (featureSet.next()) {

            if (isTesting) {
                int extractedFeatureCount = 0;
                for (int counter : extractedFeaturesLocal) {
                    trainingExamples[exampleNumber][extractedFeatureCount] = sigmoid(featureSet.getDouble(counter));
                    // System.out.print(trainingExamples[exampleNumber][extractedFeatureCount]+" ");
                    extractedFeatureCount++;
                }
                if (userFeedback != null) {
                    userFeedback[exampleNumber] = featureSet.getDouble(ConstantValues.CONTEXTINFO_PAYOFF); //TODO:Think of a better way to do this
                }
            } else {
                int extractedFeatureCount = 0;
                //LOGGER.log(Level.INFO, "feature construction not testing");
                //System.out.print("Training example value ");
                int noColumns = featureSet.getMetaData().getColumnCount();
                for (int i = 2; i < noColumns; i++) { //TODO: Change this to a configuration param
                    double val = sigmoid(featureSet.getDouble(i));
                            // TODO: For Ocean I might need this. sigmoid(featureSet.getDouble(i));//(featureSet.getDouble(i) - meanVals[0][extractedFeatureCount]) / stdDevVals[0][extractedFeatureCount];
                   //System.out.print(val);
                    trainingExamples[exampleNumber][extractedFeatureCount] = val;  //scale the trainingExamples TODO:Test the implementation
                    extractedFeatureCount++;

                }
                if (userFeedback != null) {
                    userFeedback[exampleNumber] = featureSet.getDouble(ConstantValues.CONTEXTINFO_PAYOFF); //TODO:Think of a better way to do this
                }
            }
            exampleNumber++;
        }
        System.out.println("");
        statementFeatureSet.close();
    }

    private void writeArmToDataBase(Arm arm, Connection connection) {
        //For storing the JSON object of the newFeatureSet will be used.
        String AaJson = getAsJSON(arm.getAa());
        String baJson = getAsJSON(arm.getba());
        String BaJson = getAsJSON(arm.getBa());

        StringBuffer updateSQL = new StringBuffer("UPDATE " + tableNameConfigGraph + " SET ");
        if (AaJson != null) {
            updateSQL.append(ConstantValues.DB_Aa + " = \"").append(AaJson).append("\",");
        }
        if (AaJson != null) {
            updateSQL.append(ConstantValues.DB_ba + " = \"").append(baJson).append("\",");
        }
        if (BaJson != null) {
            updateSQL.append(ConstantValues.DB_Ba + " = \"").append(BaJson).append("\",");
        }

        updateSQL.append(ConstantValues.CONFIGGRAPH_EXAMPLES + " = ").append(arm.getNumberOfTrainingExamples()).append(",");
        updateSQL.append(ConstantValues.CONFIGGRAPH_IS_NEW + " = ").append(arm.isNew());
        updateSQL.append(" WHERE " + ConstantValues.CONFIGGRAPH_ID + " = ").append(arm.getArmID());

        insertToDB(updateSQL.toString(), connection);

    }

    private void insertToDB(String insertSQL, Connection connection) {

        try {
            //Connection connection = DriverManager.getConnection(ConstantValues.DATABASE_HOST_IP + databaseName, "ocean", "mit123");
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate(insertSQL); //TODO:Validate the return value
            //connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getAsJSON(Object newFeatureSet) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = null;
        try {
            if (newFeatureSet != null) {
                jsonString = mapper.writeValueAsString(newFeatureSet);
            } else {
                jsonString = "";
            }

        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Error getting the feature set as a JSON object");
            e.printStackTrace();
        }
        return jsonString;
    }

    public void LinucbDisjoint(double[] xta, Arm arm) {
        // LOGGER.log(Level.INFO, "LinucbDisjoint");

        if (arm.isNew()) {
            arm.setAa(getIdentityMatrix(featureDimensionArm));
            arm.setba(new double[featureDimensionArm]);
        }

        Matrix AaI = getinvertedMatrixArray(arm);
        Matrix ba = new Matrix(arm.getba(), arm.getba().length);

        /**The response vector is [D*M][M].  it is the multiplication of transpose of design matrix with the user
         * OptimumActionInputLocation provided to each trial M */

        //TODO use gradient descent here.
        Matrix theta = AaI.times(ba);

        double meanPayOff = getRowVectorColumnVectorMultiplication(theta, xta);
        System.out.print(" meanPayOff " + meanPayOff);
        double standardDeviation =0;
        if (localAlpha!=0){
            //System.out.println("APLPHA IS NOT ZERO ");
            standardDeviation = calculateUCB(xta, AaI); //changed from arm.getA0()
        }
        System.out.print(" standardDeviation " + standardDeviation);
        double payOffForArm = meanPayOff + standardDeviation;
        //double payOffForArm = (ConstantValues.BETA*meanPayOff) + standardDeviation;
        System.out.print(" payOffForArm " + payOffForArm);
        arm.setPayoff(payOffForArm);

    }

    private void LinucbHybrid(double[] _xta, double[] _zta, Arm arm, Matrix beta) throws Exception {
        if (arm.isNew()) {
            arm.setAa(getIdentityMatrix(featureDimensionArm));
            arm.setba(new double[featureDimensionArm]);
            arm.setBa(new double[featureDimensionArm][featureDimensionCommon]);
        }
        Matrix Aa = new Matrix(arm.getAa());
        Matrix AaI = Aa.inverse();

        Matrix matTemp = get_baMinusBaTimeBeta(arm, beta); //Line 12 Algorithm 2 in Li et al.
        Matrix theta_hybrid = AaI.times(matTemp);

        double meanUserPayoff = getRowVectorColumnVectorMultiplication(beta, _zta);
        double meanArmPayoff = getRowVectorColumnVectorMultiplication(theta_hybrid, _xta);
        double standardDeviation =0;
        if (localAlpha!=0){
            standardDeviation = calculateUCBHybrid(_xta, _zta, Aa, arm.getBa(), commonArtifact.getA0());// [Li et al., 2011] algo line 13
            if (standardDeviation<0){
                //throw new Exception("Standard deviation is less than zeo");
                standardDeviation=0;
                System.out.println("###WARNINING MINUS STD DEV ###");
                LOGGER.log(Level.WARNING, "A minus standard deviation is encountered");
            }
        }

        double standard_dev= localAlpha * Math.sqrt(standardDeviation);
        double payoffForArm = meanUserPayoff + meanArmPayoff + standard_dev;
        System.out.println(" payOffForArm " + payoffForArm + " meanUser " + meanUserPayoff + " meanArm " + meanArmPayoff + " stdDev " + standard_dev);
        arm.setPayoff(payoffForArm);
    }

    private Arm findArmWithMaxPayoff(Arm arm, Arm maxPayOffArm) {
        Arm newMaxPayOffArm= maxPayOffArm;
        if (new Arm().getPayOffComparator().compare(arm, maxPayOffArm)==1) {
            newMaxPayOffArm = arm;
            maxPayOffArmExamples = arm.getNumberOfTrainingExamples();
        }
        if (isRanked) {
            //arm.setPayoff(payOffForArm);
            rankedList.add(arm);
        }
        return newMaxPayOffArm;
    }

    private Matrix getinvertedMatrixArray(Arm arm) {
        Matrix armFeatureMatrix = new Matrix(arm.getAa());
        return armFeatureMatrix.inverse();
    }

    private double getUserFeedback(boolean isTesting, double maxPaidOffArmLoc, boolean isSelectedArm) {
        //Get the OptimumActionInputLocation from the user for the recomendation made by LinUCB
        //TODO:Implement a better scheme to get this value from the user. PLus can consider cross checking with the data.
        double feedbackVal = 0;


        if (isTesting) {
            long startTime = System.nanoTime();

            if (isArtificial) {
                System.out.println("No Real device. Since the reward is simulated. Picked Arm is " + maxPaidOffArmLoc);
                feedbackVal = artificialDataSimulator.getSimulatedReward(simulatedNewFeatureForArtificialFeedback, simulatedThetaList.get(maxPaidOffArmLoc), isRewardInaccurate, simulatedCommonBeta, simulatedNewUserForArtificialFeedback);

            } else {
                double realDevice = inputRecordArm[ConstantValues.OptimumActionInputLocation]; //249 is the device in the Right Arm. If the data set is changing this needs to change.
                if (randomized) {
                    maxPaidOffArmLoc = randomNumberGenerator(501, 523); //Need to change the range according to the data set
                    long endTime = System.nanoTime();
                    long duration = endTime - startTime;
                    System.out.println("maxPayOfArmID selected randomly " + maxPaidOffArmLoc + " selection time " + duration);
                }
                System.out.println("realDevice =" + realDevice);
                if (maxPaidOffArmLoc == realDevice) {
                    feedbackVal = 1;
                    //feedbackVal = artificialDataSimulator.getSimulatedReward(simulatedNewFeatureForArtificialFeedback, simulatedThetaList.get(maxPaidOffArmLoc), false, simulatedCommonBeta,simulatedNewUserForArtificialFeedback);
                    if (isSelectedArm) {
                        numberOfCorrectRecommendations++;
                        avgReward++;
                    }

                } else {
                    feedbackVal = 0;
                    //feedbackVal = artificialDataSimulator.getSimulatedReward(simulatedNewFeatureForArtificialFeedback, simulatedThetaList.get(maxPaidOffArmLoc), isRewardInaccurate, simulatedCommonBeta, simulatedNewUserForArtificialFeedback);
                }
            }

        } else {
            System.out.println("Enter your OptimumActionInputLocation");
            Scanner scanner = new Scanner(System.in);
            String feedback = scanner.nextLine();//System.console().readLine();
            feedbackVal = Double.parseDouble(feedback);

        }
        return feedbackVal;
    }

    public Arm writeNewFeatureToDataBase(double payoff, boolean isTesting, double[] xta_s, double r,
                                         Connection connection, boolean isHybrid, double[] zta_s) throws SQLException {

        //TODO: No need to write to database. COnfirm!
        Statement statement = connection.createStatement();
        StringBuffer insertSQL = new StringBuffer("INSERT INTO " + tableNameContextInfo + " VALUES (");
        Arm arm = null;
        try {
            //prepareInsertSQL(payoff, isTesting, xta_f, r, insertSQL);

            //int resultSet = statement.executeUpdate(insertSQL.toString());//TODO: Check out the return value and handle exceptions.
            if (isHybrid){
                //write_zta_f_ToDatabase(payoff, isTesting, zta_f, r, connection);
            }

            arm = calculateArmFeatureSetResponseValues(r, connection, xta_s, payoff, isHybrid,zta_s);
            arm.setArmID(r);
            //if (resultSet == -1) {
             //   System.out.println("DATABASE WRITE UNSUCCESSFUL!");
             //   System.exit(-1);
            //}

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            statement.close();
        }

        return arm;
    }

    private boolean write_zta_f_ToDatabase(double realValuedPayOff, boolean isTesting, double[] inputRecordUser, double maxPayOffArmID, Connection connection) throws SQLException {
        //TODO: Make the two tables compatile with proper keys
        Statement statement = connection.createStatement();
        StringBuffer insertSQL = new StringBuffer("INSERT INTO " + tableNameUserInfo + " VALUES (");
        prepareInsertSQL(realValuedPayOff, isTesting, inputRecordUser, maxPayOffArmID, insertSQL);
        System.out.println(insertSQL);
        int insertIsSuccess = statement.executeUpdate(insertSQL.toString());
        statement.close();
        return insertIsSuccess != -1;
    }

    private void prepareInsertSQL(double payoff, boolean isTesting, double[] recordTobeSaved, double feedbackForArm, StringBuffer insertSQL) {
        if (isTesting) {
            boolean isGoingCrazy = true; //TODO: I dont know what I was thinking here.
            recordTobeSaved = recordTobeSaved;
            if (isArtificial || isGoingCrazy) {
                //Calculate the paypff as in Ku-Chun
                insertSQL.append(feedbackForArm + ",");
                int counter = 0;
                for (double val : recordTobeSaved) { //TODO:This way of saving to the database is crappy. Need to change it.
                    if (counter != ConstantValues.OptimumActionInputLocation) {
                        insertSQL.append(val);
                        insertSQL.append(",");
                    }
                    counter++;
                }
                insertSQL.append(payoff + ")");
            } else {


                double tmpRealDevice = recordTobeSaved[ConstantValues.OptimumActionInputLocation];
                recordTobeSaved[ConstantValues.OptimumActionInputLocation] = feedbackForArm;
                for (double val : recordTobeSaved) { //TODO:This way of saving to the database is crappy. Need to change it.
                    insertSQL.append(val);
                    insertSQL.append(",");
                }
                insertSQL.append(payoff + ")");
                recordTobeSaved[ConstantValues.OptimumActionInputLocation] = tmpRealDevice;
            }

        } else {
            insertSQL.append(feedbackForArm + " , ");
            int i = 0;
            for (double val : recordTobeSaved) { //TODO:This way of saving to the database is crappy. Need to change it.
                insertSQL.append(val);
                //if (i != recordTobeSaved.length - 1) {
                    insertSQL.append(",");
               // }
                i++;

            }
            insertSQL.append(payoff+")");
        }
    }

    private double[] getSquareMatrixColumnVectorMultiplication(double[][] square, double[] column) {
        double[] theta;
        Matrix matSquare = new Matrix(square);
        Matrix matRespVector = new Matrix(column, column.length);
        theta = matSquare.times(matRespVector).getColumnPackedCopy();
        return theta;
    }

    private double getRowVectorColumnVectorMultiplication(Matrix column, double[] row) {
        // multiplication needs to be row*column
        Matrix f = new Matrix(row, row.length);
        Matrix fT = f.transpose();
        Matrix t = fT.times(column);
        assert(t.getColumnDimension()==1 && t.getRowDimension()==1);
        return t.get(0,0);
    }

    public double[][] getIdentityMatrix(int dimension) {
        double[][] identityMatrix;
        identityMatrix = Matrix.identity(dimension, dimension).getArray();
        return identityMatrix;
    }

    public double[] getDoubleArrayFromStringArray(String inputFeatureString, boolean isUser, double[] newFeature) {

        String[] inputFeatureStringArray = inputFeatureString.split(",");
        System.out.println("inputFeatureStringArray.length" + inputFeatureStringArray.length);
        int extractedFeatureCounter = 0;
        int j = 0;
        for (String value : inputFeatureStringArray) {
            if (value.equals("NaN")) {
                value = "0";
            }
            if (isUser) {
                inputRecordUser[j] = Double.parseDouble(new DecimalFormat("#.00000000").format(Double.parseDouble(value)));
                if (extractedFeaturesUsers.contains(j + 1)) {
                    newFeature[extractedFeatureCounter] = Double.parseDouble(value);
                    extractedFeatureCounter++;
                }
            } else {
                inputRecordArm[j] = Double.parseDouble(value);
                if (extractedFeatures.contains(j + 1)) {
                    newFeature[extractedFeatureCounter] = Double.parseDouble(value);
                    extractedFeatureCounter++;
                }
            }

            j++;
        }
        /*
        for (int i = 0; i < featureDimensionMaxNumber; i++) {

            if (inputFeatureStringArray[i].equals("NaN")) {
                inputFeatureStringArray[i] = "0";

            }
            inputRecordArm[i] = Double.parseDouble(inputFeatureStringArray[i]);
            if (extractedFeatures.contains(i + 1)) {
                newFeature[extractedFeatureCounter] = Double.parseDouble(inputFeatureStringArray[i]);
                extractedFeatureCounter++;

            }
        }*/
        return newFeature;
    }

    public static double sigmoid(double x) {
        //System.out.print("sigmoid..");
        //return (1/( 1 + Math.pow(Math.E,(-1*x))));
        return (x / Math.sqrt(1 + Math.pow(x, 2)));
        //return x;
    }

    public double[] getScaledFeature(double[] feature) {
        if (feature==null){
            return null;
        }
        if (isArtificial){
            return feature;
        }
        double[] scaledFeature = new double[feature.length];
        for (int i = 0; i < feature.length; i++) {
            scaledFeature[i] = sigmoid(feature[i]);
        }
        return scaledFeature;
    }

    public int randomNumberGenerator(int start, int end) {
        int r = random.nextInt(end - start) + start;
        return r;
    }

    public double[] getSecondaryUserInformation(String cluster_number, HashMap<String, ArrayList<double[]>> cluster_details) {
        System.out.println("getSecondaryUserInformation: cluster number "+cluster_number+ " cluster details "+cluster_details.size());
        ArrayList<double[]> tmpArray=cluster_details.get(cluster_number);
        int index = random.nextInt(tmpArray.size());
        System.out.println("Index " + index);
        double[] altUser = tmpArray.get(index);
        return altUser;
    }

    /*public double getAltrRewardForIoTSetting(double altArm) {
        double altrRewardForIoTSetting=0;
        double[] similar_arms=altRewardMapLocal.get(maxPayOffArmID);
        for (double arm :similar_arms){
            if (arm==altArm){
                altrRewardForIoTSetting=1;
            }
        }
        return altrRewardForIoTSetting;
    }*/

    public void writeNewFeatureToDataBaseHybridModelServerCall(HashMap<Double, Double> realValuedPayOff, boolean isTesting, double[] xta, HashMap<Double, ArrayList<Double>> userArmVals) {
        try {
            for (Double a: realValuedPayOff.keySet()) {
                Connection connection = getDBConnection();
                double[] zta =  getZta(userArmVals.get(a), xta);
                writeNewFeatureToDataBaseHybridModel(realValuedPayOff.get(a), isTesting, xta, zta, a, connection);
                connection.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void writeNewFeatureToDataBaseDisjointModelServerCall(HashMap<Double, Double> realValuedPayOff,
                                                                 boolean isTesting, double[] inputRecordArm){
        Connection connection= getDBConnection();
        try {
            for (Double a: realValuedPayOff.keySet()){
                Arm armw=writeNewFeatureToDataBase(realValuedPayOff.get(a), isTesting, inputRecordArm, a, connection, false, null);
                writeArmToDataBase(armw, connection);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        /*BanditSolver banditSolver = new BanditSolver("SIMULATION", "simulation", "configgraphs", "contextinfo", 13, 14, 8, "userinfo");
        Connection connection=banditSolver.getDBConnection();
        banditSolver.setA0B0(connection);
        banditSolver.commonArtifact.getA0().print(10,10);
        banditSolver.commonArtifact.getb0().print(0,0);
        System.out.println(banditSolver.commonArtifact.getA0().get(0,0));

        HashMap<Double, Double> hmap = new HashMap<Double, Double>();

        hmap.put(5.0, 3.3);
        hmap.put(11.0,1.6);
        hmap.put(4.0, 5.4);
        hmap.put(77.0, 8.5);
        hmap.put(9.0, 9.0);
        hmap.put(66.0, 2.0);
        hmap.put(0.0, 5.0);
        System.out.println(hmap);
        System.out.println(sortByValues(hmap));


        String s = "OUTPUT:LIGHT;OUTPUT:AUDIO";
        System.out.println(getFilteringSQLCondition(s));
        */

        ArrayList<String> test = new ArrayList<String>();
        test.add("apple");
        test.add("bat");
        System.out.println(test.toString().replace("[","").replace("]",""));
    }
}

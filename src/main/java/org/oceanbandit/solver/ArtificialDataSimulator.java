package org.oceanbandit.solver;

import Jama.Matrix;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Nirandika Wanigasekara on 20/1/2016.
 */
public class ArtificialDataSimulator {
    private Random random;
    private Random error_randome;


    public ArtificialDataSimulator() {
        random = new Random(50);
        //random.setSeed(12);
        error_randome = new Random(10);
        // error_randome.setSeed(10);

    }

    private double nextNormalContext() {
        double aLong= random.nextDouble()*2-1;
        //System.out.println(aLong);
        return aLong;

    }

    private double nextUniformCoefficient() {
        return random.nextDouble();
    }

    private double[] getUniVector(int featureDim) {
        double vector_sum = 0;
        double[] vector = new double[featureDim];
        for (int i = 0; i < vector.length; i++) {
            double cell_value = nextUniformCoefficient();
            vector[i] = cell_value;
            vector_sum = vector_sum + (Math.pow(cell_value, 2));
        }
        double vector_magnitude = Math.sqrt(vector_sum);
        for (int i = 0; i < vector.length; i++) {
            vector[i] = vector[i] / vector_magnitude;
        }
        return vector;
    }

    public double[] getContextVector(int contextDim) {
        double[] context = new double[contextDim];
        for (int i = 0; i < context.length; i++) {
            context[i] = nextNormalContext();
        }
        Matrix mat_context = new Matrix(context, context.length);
        double l2_norm = mat_context.norm2();
        while (l2_norm >= 1) {
            context = new double[contextDim];
            for (int i = 0; i < context.length; i++) {
                context[i] = nextNormalContext();
            }
            mat_context = new Matrix(context, context.length);
            l2_norm = mat_context.norm2();
        }
        System.out.print("Context ");
        mat_context.print(2, 5);
        return mat_context.getColumnPackedCopy();

    }

    public HashMap<Double, double[]> getUnitVectorSetForActions(int numberOfActions, int armFeatureDim) {
        HashMap<Double, double[]> unitVetorSet = new HashMap<Double, double[]>();
        for (double i = 1; i < numberOfActions + 1; i++) {
            unitVetorSet.put(i, getUniVector(armFeatureDim));
        }
        return unitVetorSet;
    }

    public double getSimulatedReward(double[] contextArm, double[] unitVector, boolean isError, double[] simulatedBeta, double[] contextUser) {
        //System.out.println(context.length + " " + unitVector.length);

        Matrix mat_context_arm = new Matrix(contextArm, contextArm.length);
        Matrix mat_cotext_user = new Matrix(contextUser, contextUser.length);
        Matrix mat_uniVector = new Matrix(unitVector, unitVector.length);
        Matrix mat_beta = new Matrix(simulatedBeta, simulatedBeta.length);
        Matrix mat_reward_arm = mat_context_arm.transpose().times(mat_uniVector);
        Matrix mat_reward_user = mat_cotext_user.transpose().times(mat_beta);
        Matrix mat_reward = mat_reward_arm.plus(mat_reward_user);

        double reward = 0;
        if (isError) {
            reward = mat_reward.get(0, 0) + (error_randome.nextDouble() * 2 - 1);
        } else {
            reward = mat_reward.get(0, 0); //+(error_randome.nextDouble()*0.1-0.05);
        }
        //mat_reward.print(1, 1);
        return reward;

    }

    public double[] getCommonVector(int userDimension) {
        double[] commonVector = getContextVector(userDimension);

        return commonVector;
    }


    public static void main(String[] args) {


        ArtificialDataSimulator artificialDataSimulator = new ArtificialDataSimulator();
        int numberOfUserFeature = 10;
        int numberOfArmFeatures = 10;
        int numberOfArms = 20;
        int numberOfUsers = 10; //I dont know how to incorporate this yet.
        int numberOfRecords = 10000;

        try {
            FileWriter fileReader = new FileWriter("artificial_dataset_hybrid_uniform_20_arms.csv");
            BufferedWriter bufferedWriter = new BufferedWriter(fileReader);
            Double maxUnitVectorKey = (double) -1;
            double[] simulatedBeta = artificialDataSimulator.getCommonVector(numberOfUserFeature);
            HashMap<Double, double[]> actionSetWithSimulatedTheta = artificialDataSimulator.getUnitVectorSetForActions(numberOfArms, numberOfArmFeatures);
            for (int i = 0; i < numberOfRecords; i++) {
                double maxReward = -100;
                double[] contextVecArm = artificialDataSimulator.getContextVector(numberOfArmFeatures);
                double[] contextVecUser= artificialDataSimulator.getContextVector(numberOfUserFeature);
                for (Double key : actionSetWithSimulatedTheta.keySet()) {

                    double reward;
                    reward = artificialDataSimulator.getSimulatedReward(contextVecArm, actionSetWithSimulatedTheta.get(key), false, simulatedBeta, contextVecUser);

                    if (maxReward < reward) {
                        maxReward = reward;
                        maxUnitVectorKey = key;
                    }
                }
                //System.out.println("Reward "+maxReward);
                for (int j = 0; j < numberOfArmFeatures; j++) {
                    bufferedWriter.write(contextVecArm[j] + ",");
                }
                bufferedWriter.write(maxUnitVectorKey + /*"," + maxReward +*/ "|");
                for (int j=0;j<numberOfUserFeature;j++){
                    bufferedWriter.write(contextVecUser[j]+",");
                }
                bufferedWriter.write(maxUnitVectorKey +/* "," + maxReward +*/ "\n");
            }
            bufferedWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}

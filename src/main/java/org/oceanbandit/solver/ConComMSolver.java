package org.oceanbandit.solver;

import org.oceanbandit.constantvalues.ConstantValues;
import org.oceanbandit.datatypes.Arm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Nirandika Wanigasekara on 18/5/2017.
 */
public class ConComMSolver {

    private HashMap<Integer, double[]> multiClassClassifier = new HashMap<Integer, double[]>();
    private Arm maxPayOffArm;
    private List<Arm> payoffList;
    private List<Arm> totalScore;
    private HashMap<String, Integer> usertasks;
    private String databaseName;
    private String tableNameConfigGraph;
    private static int counter = 0;
    private static double epsilon = 0.2;

    public ConComMSolver(Arm _maxPayoffArm, List<Arm> _payoffList, String[] _tasks, String _db, String _dbTable) {
        //populateMulticlassClassifier();
        maxPayOffArm = _maxPayoffArm;
        payoffList = _payoffList;
        totalScore = new ArrayList<Arm>();
        generateUserTasks(_tasks);
        //Arrays.sort(usertasks);
        databaseName = _db;
        tableNameConfigGraph = _dbTable;
        counter++;
        if (counter % 10 == 0 && epsilon > 0.01) {
            epsilon = epsilon - 0.01;
        }
    }

    private void generateUserTasks(String[] tasks) {
        usertasks = new HashMap<String, Integer>();
        for (String task : tasks) {
            if (!usertasks.containsKey(task)) {
                usertasks.put(task, 0);
            }
            usertasks.put(task, usertasks.get(task) + 1);

        }
    }

    public ArrayList<String> selectSmartCompositeService() {
        double val = new Random(100).nextDouble();
        ArrayList<Arm> scs = new ArrayList<Arm>();
        if (val < epsilon) {
            //explore
            Collections.sort(payoffList, new Arm().getPayOffComparator());
            for (Arm a : payoffList) {
                if (isTaskComplete()){
                    break;
                }
                if (usertasks.containsKey(a.getConfiguGraph())){
                    int c = usertasks.get(a.getConfiguGraph());
                    scs.add(a);
                    c--;
                    if (c==0){
                        usertasks.remove(a.getConfiguGraph());
                    }else {
                        usertasks.put(a.getConfiguGraph(), c);
                    }
                }
            }
        } else {
            //exploit
            int container = getInteractionEnablingContainer();
            calculateCompatibilityScore(container);
            scs.add(maxPayOffArm);
            removeUnwantedArms(maxPayOffArm);
            while (!isTaskComplete()) {
                Arm maxTotalScoreArm = getArmWithMaxTotalScore();
                if (maxTotalScoreArm != null) {
                    scs.add(maxTotalScoreArm);
                    removeUnwantedArms(maxTotalScoreArm);
                } else {
                    return getStringSCS(scs);
                }
            }
        }
        return getStringSCS(scs);
    }

    private ArrayList<String> getStringSCS(ArrayList<Arm> arms) {
        ArrayList<String> strArms = new ArrayList<String>();
        for (Arm arm : arms) {
            strArms.add(Double.toString(arm.getArmID()));
        }
        return strArms;
    }

    private Arm getArmWithMaxTotalScore() {
        if (totalScore.isEmpty()) {
            return null;
        }
        Collections.sort(totalScore, new Arm().getTotalScoreComparator());
        return totalScore.get(0);
    }

    /**
     * Removes the unwanted arms. Unwanted means there is no point in considering these servies for SCS because those
     * capabilities are already captured.
     * TODO: Right now I just remove selected arms but that should be based on covered capabilities.
     */

    private void removeUnwantedArms(Arm a) {
        System.out.println("Total score size " + totalScore.size());
        ArrayList<Arm> removeList = new ArrayList<Arm>();

        totalScore.remove(a);

        if (usertasks.containsKey(a.getConfiguGraph())) {
            int c = usertasks.get(a.getConfiguGraph());
            c--;
            if (c == 0) {
                for (Arm x : totalScore) {
                    if (x.getConfiguGraph().equals(a.getConfiguGraph())) {
                        removeList.add(x);
                    }
                }
                usertasks.remove(a.getConfiguGraph());
                totalScore.removeAll(removeList);

            } else {
                usertasks.put(a.getConfiguGraph(), c);
            }
        }

    }

    private boolean isTaskComplete() {
        return usertasks.isEmpty();
    }

    /**
     * Calculates the degree to which each service in payoffList complementes the most contextually relevant service
     * maxPayOffArm
     * TODO: Remove the hard coded values
     */
    private void calculateCompatibilityScore(int c) {
        double tot;
        if (c == 1) {
            for (Arm arm : payoffList) {
                tot = ConstantValues.getContainerOnePr(arm.getArmID()) + arm.getPayoff();
                arm.setTotalScore(tot);
                totalScore.add(arm);
            }
        } else {
            for (Arm arm : payoffList) {
                tot = ConstantValues.getContainerTwoPr(arm.getArmID()) + arm.getPayoff();
                arm.setTotalScore(tot);
                totalScore.add(arm);
            }
        }
    }


    /**
     * Finds to which interaction-enabling container the most contextual relevant service belongs to
     */
    private int getInteractionEnablingContainer() {
        return (ConstantValues.getContainerOnePr(maxPayOffArm.getArmID()) > ConstantValues.getContainerTwoPr(maxPayOffArm.getArmID())) ? 1 : 2;
    }


    /*TODO Once the classifier values are known use this*/
    private void populateMulticlassClassifier() {
        multiClassClassifier.put(1, new double[]{0, 0});
    }

    private Connection getDBConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(ConstantValues.DATABASE_HOST_IP + databaseName, "ocean", "mit123");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}

package org.oceanbandit.dataprocessor;

import java.io.*;

/**
 * Created by Nirandika Wanigasekara on 6/5/2015.
 */
public class R6AProcessor {


    public static void main(String[] args) {
        FileReader fileReader = null;
        File results = new File("R6ATrainingSet.txt");

        try {
            fileReader = new FileReader("ydata-fp-td-clicks-v1_0.20090501");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(results));
            boolean isFull = true;
            int n=0;
            while (isFull&&n<50000) {
                String event = bufferedReader.readLine();
                if (event != null) {
                    String[] atomEvent = event.split("\\|");
                    String[] atomResult = atomEvent[0].split(" ");
                    String clikedArticleId = atomResult[1];
                    String feedback = atomResult[2];
                    bufferedWriter.write(clikedArticleId+" "+feedback+" ");

                    for (int i = 2; i < 22; i++) {
                        String[] atomeFeatures = atomEvent[i].split(" ");
                        if (atomeFeatures[0].equals(clikedArticleId)) {
                            for (int j = 1; j < 7; j++) {
                                String[] atomeAtribute = atomeFeatures[j].split(":");
                                bufferedWriter.write(atomeAtribute[1]+" ");
                            }

                        }
                    }

                } else {
                    isFull = false;
                }
                bufferedWriter.write("\n");
                n++;
            }

            bufferedWriter.close();
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createFeatures(){
        FileReader fileReader = null;
        File results = new File("R6ATrainingSet.txt");

        try {
            fileReader = new FileReader("ydata-fp-td-clicks-v1_0.20090501");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(results));
            boolean isFull = true;
            int n=0;
            while (isFull&&n<100000) {

                String event = bufferedReader.readLine();
                n++;
                if(n>=50000){

                    if (event != null) {
                        String[] atomEvent = event.split("\\|");
                        String[] atomResult = atomEvent[0].split(" ");
                        String clikedArticleId = atomResult[1];
                        String feedback = atomResult[2];
                        bufferedWriter.write(clikedArticleId+" "+feedback+" ");

                        for (int i = 2; i < 22; i++) {
                            String[] atomeFeatures = atomEvent[i].split(" ");
                            if (atomeFeatures[0].equals(clikedArticleId)) {
                                for (int j = 1; j < 7; j++) {
                                    String[] atomeAtribute = atomeFeatures[j].split(":");
                                    bufferedWriter.write(atomeAtribute[1]+" ");
                                }

                            }
                        }

                    } else {
                        isFull = false;
                    }
                    bufferedWriter.write("\n");

                }

            }

            bufferedWriter.close();
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
